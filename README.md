A dosimetric comparison between ICRP and ORNL phantoms from exposure to 137Cs contaminated soil.
Android app to determine the equivalent and effective dose values from Cs-137 contaminated soil using conversion coefficients for different organs.
Developed by Cyric, Tohoku University, Japan
By: Hiroshi Watabe and Mehrdad S. Beni

Our research paper DOI link: https://doi.org/10.1016/j.radphyschem.2023.110878